<?php


class TicketController extends BaseController
{

    protected $answer = [];
    private $ticketModel;

    public function __construct()
    {
        $this->ticketModel = new Ticket();
    }

    public function main($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method){
            case 'GET':
                $this->get($id);
                break;
            case 'POST':
                $this->post();
                break;
            case 'PUT':
                $this->put($id);
                break;
            case 'DELETE':
                $this->delete($id);
                break;
            default:
                $this->showNotAllowed();
        }
    }

    private function get($id) {
        if($id > 0){
            // метод для получения ticket с определенным id
            $ticket = $this->ticketModel->getTicket($id);
            if($ticket){
                $this->answer[] = $ticket;
                $this->sendAnswer();
            } else {
                $this->showNotFound();
            }
        } else {
            // метод для получения всех ticket
            $tickets = $this->ticketModel->getTickets();
            $this->answer = $tickets;
            $this->sendAnswer();
        }
    }

    private function post() {
        $parameters = json_decode(file_get_contents("php://input"), true);
        $newTicketId = $this->ticketModel->addTicket($parameters);
        // TODO: проверка получилось ли добавить
        $this->answer = 'ticket успешно добавлен, ID: ' . $newTicketId;
        $this->sendAnswer();
    }

    private function put($id) {
        $parameters = json_decode(file_get_contents("php://input"), true);
        $editedTicketId = $this->ticketModel->editTicket($id, $parameters);
        $this->answer = 'ticket успешно отредактирован, ID: ' . $id;
        $this->sendAnswer();
    }

    private function delete($id) {
        $this->ticketModel->deleteTicket($id);
        $this->answer = 'ticket успешно удален, ID: ' . $id;
        $this->sendAnswer();
    }

}