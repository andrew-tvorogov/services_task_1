<?php


abstract class BaseController
{

    protected $answer;

    abstract function main($id);

    protected function sendAnswer() {
        header('HTTP/1.1 200 OK');
        header('Content-type: application/json');
        //echo json_encode($this->answer);
        echo json_encode($this->answer, JSON_UNESCAPED_UNICODE);
    }

    protected function showBadRequest() {
        header('HTTP/1.1 400 Bad Request');
    }

    protected function showNotFound() {
        echo '404 NOT FOUND';
        header('HTTP/1.1 404 Not Found');
    }

    protected function showNotAllowed() {
        header('HTTP/1.1 405 Method Not Allowed');
    }


}