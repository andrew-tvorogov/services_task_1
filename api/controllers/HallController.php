<?php


class HallController extends BaseController
{

    protected $answer = [];
    private $hallModel;

    public function __construct()
    {
        $this->hallModel = new Hall();
    }

    public function main($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method){
            case 'GET':
                $this->get($id);
                break;
            case 'POST':
                $this->post();
                break;
            case 'PUT':
                $this->put($id);
                break;
            default:
                $this->showNotAllowed();
        }
    }

    private function get($id) {
        if($id > 0){
            // метод для получения hall с определенным id
            $hall = $this->hallModel->getHall($id);
            if($hall){
                $this->answer[] = $hall;
                $this->sendAnswer();
            } else {
                $this->showNotFound();
            }
        } else {
            // метод для получения всех halls
            $halls = $this->hallModel->getHalls();
            $this->answer = $halls;
            $this->sendAnswer();
        }
    }

    private function post() {
        echo 'inside add hall';
    }

    private function put($id) {
        echo 'inside edit hall';
    }


}