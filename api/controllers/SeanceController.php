<?php


class SeanceController extends BaseController
{

    protected $answer = [];
    private $seanceModel;

    public function __construct()
    {
        $this->seanceModel = new Seance();
    }

    public function main($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method){
            case 'GET':
                $this->get($id);
                break;
            case 'POST':
                $this->post();
                break;
            case 'PUT':
                $this->put($id);
                break;
            case 'DELETE':
                $this->delete($id);
                break;
            default:
                $this->showNotAllowed();
        }
    }
	
	public function date($requestedDate){
		//echo 'inside date';		
		$method = $_SERVER['REQUEST_METHOD'];
        switch ($method){
            case 'GET':
                $this->getByDate($requestedDate);
                break;       
            default:
                $this->showNotAllowed();
        }		
	}

    public function cinema($requestedCinemaId){
        //echo 'inside date';
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method){
            case 'GET':
                $this->getByCinemaId($requestedCinemaId);
                break;
            default:
                $this->showNotAllowed();
        }
    }

    private function get($id) {
        if($id > 0){
            // метод для получения сеанса с определенным id
            $seance = $this->seanceModel->getSeance($id);
            if($seance){
                $this->answer[] = $seance;
                $this->sendAnswer();
            } else {
                $this->showNotFound();
            }
        } else {
            // метод для получения всех cinema
            $seances = $this->seanceModel->getSeances();
            $this->answer = $seances;
            $this->sendAnswer();
        }
    }
	
	private function getByDate($requestedDate){
        if($requestedDate){
            $response = $this->seanceModel->getSeancesByDate($requestedDate);
        } else {
            //$response = array(1, 2, 3);
            $response = $this->seanceModel->getAvailableDates();
        }
        //$response = array($requestedDate, 2, 3);
			$this->answer = $response;
            $this->sendAnswer();
	}

    private function getByCinemaId($cinemaId){
        if($cinemaId){
            $response = $this->seanceModel->getSeancesByCinemaId($cinemaId);
        } else {
            $response = $this->seanceModel->getSeances();
        }
        $this->answer = $response;
        $this->sendAnswer();
    }

//    private function post() {
//        $parameters = json_decode(file_get_contents("php://input"), true);
//        $newCinemaId = $this->cinemaModel->addCinema($parameters);
//        // TODO: проверка получилось ли добавить
//        $this->answer = 'cinema успешно добавлен, ID: ' . $newCinemaId;
//        $this->sendAnswer();
//    }

//    private function put($id) {
//        $parameters = json_decode(file_get_contents("php://input"), true);
//        $editedCinemaId = $this->cinemaModel->editCinema($id, $parameters);
//        $this->answer = 'cinema успешно отредактирован, ID: ' . $id;
//        $this->sendAnswer();
//    }

//    private function delete($id) {
//        $this->cinemaModel->deleteCinema($id);
//        $this->answer = 'cinema успешно удален, ID: ' . $id;
//        $this->sendAnswer();
//    }

}