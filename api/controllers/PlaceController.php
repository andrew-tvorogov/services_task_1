<?php


class PlaceController extends BaseController
{

    protected $answer = [];
    private $placeModel;

    public function __construct()
    {
        $this->placeModel = new Place();
    }

    public function main($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method){
            case 'GET':
                $this->get($id);
                break;
            case 'POST':
                $this->post();
                break;
            case 'PUT':
                $this->put($id);
                break;
            case 'DELETE':
                $this->delete($id);
                break;
            default:
                $this->showNotAllowed();
        }
    }
	
    public function hall($requestedHallId){
        //echo 'inside hall';
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method){
            case 'GET':
                $this->getByHallId($requestedHallId);
                break;
            default:
                $this->showNotAllowed();
        }
    }
	
	public function seance($requestedSeanceId){        
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method){
            case 'GET':
                $this->getBySeanceId($requestedSeanceId);
                break;
            default:
                $this->showNotAllowed();
        }
    }

    private function get($id) {
        if($id > 0){
            // метод для получения места с определенным id
            $response = $this->placeModel->getPlace($id);
            if($response){
                $this->answer[] = $response;
                $this->sendAnswer();
            } else {
                $this->showNotFound();
            }
        } else {
            // метод для получения всех мест
            $response = $this->placeModel->getPlaces();
            $this->answer = $response;
            $this->sendAnswer();
        }
    }

    private function getByHallId($hallId){
        if($hallId){
            $response = $this->placeModel->getPlacesByHallId($hallId);
        } else {
            $response = $this->placeModel->getPlaces();
        }
        $this->answer = $response;
        $this->sendAnswer();
    }
	
	private function getBySeanceId($seanceId){
        if($seanceId){
            $response = $this->placeModel->getPlacesBySeanceId($seanceId);
        } else {
            $response = $this->placeModel->getPlaces();
        }
        $this->answer = $response;
        $this->sendAnswer();
    }
	
	
	

//    private function post() {
//        $parameters = json_decode(file_get_contents("php://input"), true);
//        $newCinemaId = $this->cinemaModel->addCinema($parameters);
//        // TODO: проверка получилось ли добавить
//        $this->answer = 'cinema успешно добавлен, ID: ' . $newCinemaId;
//        $this->sendAnswer();
//    }

//    private function put($id) {
//        $parameters = json_decode(file_get_contents("php://input"), true);
//        $editedCinemaId = $this->cinemaModel->editCinema($id, $parameters);
//        $this->answer = 'cinema успешно отредактирован, ID: ' . $id;
//        $this->sendAnswer();
//    }

//    private function delete($id) {
//        $this->cinemaModel->deleteCinema($id);
//        $this->answer = 'cinema успешно удален, ID: ' . $id;
//        $this->sendAnswer();
//    }

}