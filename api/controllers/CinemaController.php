<?php


class CinemaController extends BaseController
{

    protected $answer = [];
    private $cinemaModel;

    public function __construct()
    {
        $this->cinemaModel = new Cinema();
    }

    public function main($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method){
            case 'GET':
                $this->get($id);
                break;
            case 'POST':
                $this->post();
                break;
            case 'PUT':
                $this->put($id);
                break;
            case 'DELETE':
                $this->delete($id);
                break;
            default:
                $this->showNotAllowed();
        }
    }

    private function get($id) {
        if($id > 0){
            // метод для получения cinema с определенным id
            $cinema = $this->cinemaModel->getCinema($id);
            if($cinema){
                $this->answer[] = $cinema;
                $this->sendAnswer();
            } else {
                $this->showNotFound();
            }
        } else {
            // метод для получения всех cinema
            $cinemas = $this->cinemaModel->getCinemas();
            $this->answer = $cinemas;
            $this->sendAnswer();
        }
    }

    private function post() {
        $parameters = json_decode(file_get_contents("php://input"), true);
        $newCinemaId = $this->cinemaModel->addCinema($parameters);
        // TODO: проверка получилось ли добавить
        $this->answer = 'cinema успешно добавлен, ID: ' . $newCinemaId;
        $this->sendAnswer();
    }

    private function put($id) {
        $parameters = json_decode(file_get_contents("php://input"), true);
        $editedCinemaId = $this->cinemaModel->editCinema($id, $parameters);
        $this->answer = 'cinema успешно отредактирован, ID: ' . $id;
        $this->sendAnswer();
    }

    private function delete($id) {
        $this->cinemaModel->deleteCinema($id);
        $this->answer = 'cinema успешно удален, ID: ' . $id;
        $this->sendAnswer();
    }

}