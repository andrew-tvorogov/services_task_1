<?php

$routes = array(

    'CinemaController' => array(
        // GET cinemas  -> получить все кинотеатры
        // GET cinemas/1 -> конкретный кинотеатр
        // POST cinemas/ {"name":"Sample Cinema Name","address":"Sample Street 4"}  -> добавить кинотеатр
        // PUT cinemas/1 {"name":"New Name","address":"New Street 1"} -> изменить кинотеатр
        // DELETE cinemas/1 -> удалить кинотеатр
        'cinemas\/*([0-9]*)' => 'main'
    ),

    'HallController' => array(
        // GET halls  -> получить все залы
        // GET halls/1 -> конкретный зал
        'halls\/*([0-9]*)' => 'main'
    ),

    'SeanceController' => array(
        // GET seances/date/2017-02-01 -> сеанс на конкретную дату
        // GET seances/dates -> все доступные даты, для которых существует сеанс
        // GET seances/cinema/1 -> все сеансы в конкретном кинотеатре
        // GET seances/1 -> конкретный сеанс
        // GET seances  -> получить все сеансы
		'seances\/date\/([0-9]{4}-[0-9]{2}-[0-9]{2})' => 'date',
        'seances\/dates\/*' => 'date',
        'seances\/cinema\/*([0-9]+)' => 'cinema',		
        'seances\/*([0-9]*)' => 'main'
    ),

    'PlaceController' => array(
        // GET places  -> все места во всех кинотеатрах
        // GET places/1 -> конкретное место
        // GET places/seance/1 -> все места конкретного сеанса
        // GET places/hall/1 -> все места конкретного зала
		'places\/seance\/*([0-9]+)' => 'seance',
		'places\/hall\/*([0-9]+)' => 'hall',
        'places\/*([0-9]*)' => 'main'
    ),

    'TicketController' => array(
        // GET tickets  -> все билеты
        // GET tickets/1 -> конкретный билет
        // POST tickets/ {"seance":"1","row":"1","number":"1","status":"1"} -> добавить новый билет
        // PUT tickets/1 {"seance":"1","row":"1","number":"1","status":"0"} -> изменить билет
        // DELETE tickets/1 -> удалить конкретный билет
        'tickets\/*([0-9]*)' => 'main'
    )

);

?>