<?php


class Place
{
    private $db;

    public function __construct()
    {
        $this->db = DB::getConnection();
    }

    public function getPlaces() {
        $query = "SELECT * FROM `place`;";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function getPlace($id) {
        $query = "SELECT * FROM `place` WHERE `place`.`ID` = $id;";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }
	
    public function getPlacesByHallId($id) {
        //$query = "SELECT * FROM `place` WHERE `ID_hall` = $id;";		
		$query = "
		SELECT `place`.`ID` AS `ID`,
						`place`.`ID_hall` AS `ID_hall`,
						`place`.`row` AS `row`,
						`place`.`number` AS `number`,
						`hall`.`name` AS `name_hall`,
						`hall`.`ID_cinema` AS `ID_cinema`
				FROM `place`
				LEFT JOIN `hall` ON `hall`.`ID` = `place`.`ID_hall`				
				WHERE `ID_hall` = $id;";
				
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function getPlacesBySeanceId($id){
        // массив всех мест зала сеанса с билетами
        // если билета на место нет, то все графы билета NULL
        // для статуса места сделал отдельную графу прямо в запросе
        $query = "
		SELECT 
		`seance`.`ID` AS `seance_id`,
		`seance`.`ID_hall` AS `seance_hall_id`,
		`seance`.`ID_movie` AS `seance_movie_id`,
		`seance`.`datetime` AS `seance_datetime`,
		`seance`.`price` AS `seance_price`,
		`hall`.`ID_cinema` AS `seance_cinema_id`,
		`hall`.`name` AS `seance_hall_name`,
		`place`.`ID` AS `seance_place_id`,
		`place`.`row` AS `seance_place_row`,
		`place`.`number` AS `seance_place_number`,
        `ticket`.`ID` AS `ticket_id`,
		/* `ticket`.`ID_seance` AS `ticket_seance_id`,
		`ticket`.`ID_status` AS `ticket_status_id`,
		`ticket`.`row` AS `ticket_row`,
		`ticket`.`number` AS `ticket_number`, */
        CASE 
        WHEN `ticket`.`ID_status` = '1' THEN '1'
		WHEN `ticket`.`ID_status` = '2' THEN '2'        
		ELSE '0'
		END AS `status`        
		FROM `seance` 
		LEFT JOIN `hall` ON `hall`.`ID` = `seance`.`ID_hall` 
		LEFT JOIN `place` ON `place`.`ID_hall` = `seance`.`ID_hall`
        LEFT JOIN `ticket` ON `ticket`.`row` = `place`.`row` AND `ticket`.`number` = `place`.`number` AND `ticket`.`ID_seance` = `seance`.`ID`
		WHERE `seance`.`ID` = $id AND `place`.`ID_hall`=`seance`.`ID_hall`
        ORDER BY `place`.`ID`;
		";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /*
	public function getPlacesBySeanceId($id){
	// массив всех мест зала сеанса
	 $query = "
		SELECT 
		`seance`.`ID` AS `seance_id`,
		`seance`.`ID_hall` AS `seance_hall_id`,
		`seance`.`ID_movie` AS `seance_movie_id`,
		`seance`.`datetime` AS `seance_datetime`,
		`seance`.`price` AS `seance_price`,
		`hall`.`ID_cinema` AS `seance_cinema_id`,
		`hall`.`name` AS `seance_hall_name`,
		`place`.`ID` AS `seance_place_id`,
		`place`.`row` AS `seance_place_row`,
		`place`.`number` AS `seance_place_number`
		FROM `seance` 
		LEFT JOIN `hall` ON `hall`.`ID` = `seance`.`ID_hall` 
		LEFT JOIN `place` ON `place`.`ID_hall`
		WHERE `seance`.`ID` = $id AND `place`.`ID_hall`=`seance`.`ID_hall`
		";
	$result = mysqli_query($this->db, $query);
    $places = mysqli_fetch_all($result, MYSQLI_ASSOC);
	
	// массив всех билетов сеанса		
		$query = "
		SELECT
		`ID` AS `ticket_id`,
		`ID_seance` AS `ticket_seance_id`,
		`ID_status` AS `ticket_status_id`,
		`row` AS `ticket_row`,
		`number` AS `ticket_number`
		FROM `ticket`
		WHERE `ticket`.`ID_seance` = $id;
		";
	$result = mysqli_query($this->db, $query);
    $tickets = mysqli_fetch_all($result, MYSQLI_ASSOC);
		
	$placesWithStatus = $places;
	
	foreach($placesWithStatus AS $index => &$place){		
		$ticketWithCurrentPlaceExist = false;
		foreach($tickets AS $ticket){			
			if ($place['seance_place_row'] == $ticket['ticket_row'] && $place['seance_place_number'] == $ticket['ticket_number']) {
				
				//$place['status'] = $ticket['ticket_status_id'];
				$place['status'] = $ticket['ticket_status_id'];
				
				$ticketWithCurrentPlaceExist = true;
			}			
		}
		if (!$ticketWithCurrentPlaceExist) {
			$place['status'] = '0';
		}		
	}
	
	return $placesWithStatus;
	}
*/

//    public function addCinema($parameters) {
//        $name = $parameters['name'];
//        $address = $parameters['address'];
//        $query = "INSERT INTO `cinema`
//                  SET `cinema`.`name` = '$name',
//                      `cinema`.`address` = '$address';
//        ";
//        mysqli_query($this->db, $query);
//        return mysqli_insert_id($this->db);
//    }
//
//    public function editCinema($id, $parameters) {
//        $name = $parameters['name'];
//        $address = $parameters['address'];
//        $query = "UPDATE `cinema` SET
//                    `cinema`.`name` = '$name',
//                    `cinema`.`address` = '$address'
//                    WHERE `cinema`.`ID` = '$id';
//                    ";
//        mysqli_query($this->db, $query);
//        return true;
//    }
//
//    public function deleteCinema($id) {
//        $query = "DELETE FROM `cinema`
//                  WHERE `cinema`.`ID` = $id";
//        mysqli_query($this->db, $query);
//        return true;
//    }
}