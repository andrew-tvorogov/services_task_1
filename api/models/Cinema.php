<?php


class Cinema
{
    private $db;

    public function __construct()
    {
        $this->db = DB::getConnection();
    }

    public function getCinemas() {
        $query = "SELECT * 
              FROM `cinema`";
        $result = mysqli_query($this->db, $query);
        $cinemas = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $cinemas;
    }

    public function getCinema($id) {
        $query = "SELECT * 
              FROM `cinema`
              WHERE `cinema`.`id` = $id";
        $result = mysqli_query($this->db, $query);
        $cinema = mysqli_fetch_assoc($result);
        return $cinema;
    }

    public function addCinema($parameters) {
        $name = $parameters['name'];
        $address = $parameters['address'];
        $query = "INSERT INTO `cinema`
                  SET `cinema`.`name` = '$name',
                      `cinema`.`address` = '$address';
        ";
        mysqli_query($this->db, $query);
        return mysqli_insert_id($this->db);
    }

    public function editCinema($id, $parameters) {
        $name = $parameters['name'];
        $address = $parameters['address'];
        $query = "UPDATE `cinema` SET
                    `cinema`.`name` = '$name',
                    `cinema`.`address` = '$address'
                    WHERE `cinema`.`ID` = '$id';
                    ";
        mysqli_query($this->db, $query);
        return true;
    }

    public function deleteCinema($id) {
        $query = "DELETE FROM `cinema`               
                  WHERE `cinema`.`ID` = $id";
        mysqli_query($this->db, $query);
        return true;
    }
}