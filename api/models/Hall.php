<?php


class Hall
{

    private $db;

    public function __construct()
    {
        $this->db = DB::getConnection();
    }

    public function getHalls() {
        $query = "SELECT 
				`hall`.`ID` AS `ID`,
				`hall`.`ID_cinema` AS `ID_cinema`,
				`hall`.`name` AS `name`,
				`cinema`.`name` AS `name_cinema`
              FROM `hall`
			  LEFT JOIN `cinema` ON `cinema`.`ID` = `hall`.`ID_cinema`";
        $result = mysqli_query($this->db, $query);
        $halls = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $halls;
    }

    public function getHall($id) {
        $query = "SELECT 
				`hall`.`ID` AS `ID`,
				`hall`.`ID_cinema` AS `ID_cinema`,
				`hall`.`name` AS `name`,
				`cinema`.`name` AS `name_cinema`
              FROM `hall`
			  LEFT JOIN `cinema` ON `cinema`.`ID` = `hall`.`ID_cinema`
              WHERE `hall`.`id` = $id";
        $result = mysqli_query($this->db, $query);
        $hall = mysqli_fetch_assoc($result);
        return $hall;
    }

}