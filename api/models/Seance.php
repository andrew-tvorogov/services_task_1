<?php


class Seance
{
    private $db;

    public function __construct()
    {
        $this->db = DB::getConnection();
    }

    public function getSeances() {
        $query = "SELECT 
`seance`.`ID` AS `ID`,
`seance`.`datetime` AS `datetime`,
`seance`.`price` AS `price`,
`movies`.`name` AS `movie_name`,
`movies`.`census` AS `census`,
`genre`.`name` AS `genre`,
`hall`.`name` AS `hall_name`,
`hall`.`ID` AS `hall_id`,       
`cinema`.`name` AS `cinema_name`
FROM `seance`
LEFT JOIN `hall` ON `hall`.`ID` = `seance`.`ID_hall`
LEFT JOIN `movies` ON `movies`.`ID` = `seance`.`ID_movie`
LEFT JOIN `genre` ON `genre`.`ID` = `movies`.`ID_genre`
LEFT JOIN `cinema` ON `cinema`.`ID` = `hall`.`ID_cinema`;
              ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function getSeance($id) {
        $query = "SELECT 
`seance`.`ID` AS `ID`,
`seance`.`datetime` AS `datetime`,
`seance`.`price` AS `price`,
`movies`.`name` AS `movie_name`,
`movies`.`census` AS `census`,
`genre`.`name` AS `genre`,
`hall`.`name` AS `hall_name`,
`hall`.`ID` AS `hall_id`,       
`cinema`.`name` AS `cinema_name`
FROM `seance`
LEFT JOIN `hall` ON `hall`.`ID` = `seance`.`ID_hall`
LEFT JOIN `movies` ON `movies`.`ID` = `seance`.`ID_movie`
LEFT JOIN `genre` ON `genre`.`ID` = `movies`.`ID_genre`
LEFT JOIN `cinema` ON `cinema`.`ID` = `hall`.`ID_cinema`
WHERE `seance`.`id` = $id;
              ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }

   public function getSeancesByDate($requestedDate) {
        $query = "SELECT 
`seance`.`ID` AS `ID`,
`seance`.`datetime` AS `datetime`,
`seance`.`price` AS `price`,
`movies`.`name` AS `movie_name`,
`movies`.`census` AS `census`,
`genre`.`name` AS `genre`,
`hall`.`name` AS `hall_name`,
`hall`.`ID` AS `hall_id`,       
`cinema`.`name` AS `cinema_name`
FROM `seance`
LEFT JOIN `hall` ON `hall`.`ID` = `seance`.`ID_hall`
LEFT JOIN `movies` ON `movies`.`ID` = `seance`.`ID_movie`
LEFT JOIN `genre` ON `genre`.`ID` = `movies`.`ID_genre`
LEFT JOIN `cinema` ON `cinema`.`ID` = `hall`.`ID_cinema`
WHERE `datetime` LIKE '$requestedDate%';
              ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }


    // получаем массив существующих дат из таблицы сеансов
    public function getAvailableDates() {
        $query = "SELECT DISTINCT(DATE_FORMAT(datetime,'%Y-%m-%d')) AS `dates` from `seance`;
              ";
        //echo $query;
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function getSeancesByCinemaId($id) {
        $query = "SELECT 
`seance`.`ID` AS `ID`,
`seance`.`datetime` AS `datetime`,
`seance`.`price` AS `price`,
`movies`.`name` AS `movie_name`,
`movies`.`census` AS `census`,
`genre`.`name` AS `genre`,
`hall`.`name` AS `hall_name`,
`hall`.`ID` AS `hall_id`,       
`cinema`.`name` AS `cinema_name`
FROM `seance`
LEFT JOIN `hall` ON `hall`.`ID` = `seance`.`ID_hall`
LEFT JOIN `movies` ON `movies`.`ID` = `seance`.`ID_movie`
LEFT JOIN `genre` ON `genre`.`ID` = `movies`.`ID_genre`
LEFT JOIN `cinema` ON `cinema`.`ID` = `hall`.`ID_cinema`
WHERE `cinema`.`ID` = $id;
              ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
	
	


//    public function addCinema($parameters) {
//        $name = $parameters['name'];
//        $address = $parameters['address'];
//        $query = "INSERT INTO `cinema`
//                  SET `cinema`.`name` = '$name',
//                      `cinema`.`address` = '$address';
//        ";
//        mysqli_query($this->db, $query);
//        return mysqli_insert_id($this->db);
//    }
//
//    public function editCinema($id, $parameters) {
//        $name = $parameters['name'];
//        $address = $parameters['address'];
//        $query = "UPDATE `cinema` SET
//                    `cinema`.`name` = '$name',
//                    `cinema`.`address` = '$address'
//                    WHERE `cinema`.`ID` = '$id';
//                    ";
//        mysqli_query($this->db, $query);
//        return true;
//    }
//
//    public function deleteCinema($id) {
//        $query = "DELETE FROM `cinema`
//                  WHERE `cinema`.`ID` = $id";
//        mysqli_query($this->db, $query);
//        return true;
//    }
}