<?php


class Ticket
{
    private $db;

    public function __construct()
    {
        $this->db = DB::getConnection();
    }

    public function getTickets() {
        $query = "SELECT * 
              FROM `ticket`";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function getTicket($id) {
        $query = "SELECT * 
              FROM `ticket`
              WHERE `ticket`.`ID` = $id";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }

    public function addTicket($parameters) {
        $seance = $parameters['seance'];
        $row = $parameters['row'];
        $number = $parameters['number'];
        $status = $parameters['status'];
        $query = "INSERT INTO `ticket`
                  SET `ticket`.`ID_seance` = '$seance',
                      `ticket`.`row` = '$row',
                      `ticket`.`number` = '$number',
                      `ticket`.`ID_status` = '$status';
        ";
        mysqli_query($this->db, $query);
        return mysqli_insert_id($this->db);
    }

    public function editTicket($id, $parameters) {
        $seance = $parameters['seance'];
        $row = $parameters['row'];
        $number = $parameters['number'];
        $status = $parameters['status'];
        $query = "UPDATE `ticket` SET
                    `ticket`.`ID_seance` = '$seance',
                    `ticket`.`row` = '$row',
                    `ticket`.`number` = '$number',
                    `ticket`.`ID_status` = '$status'
                    WHERE `ticket`.`ID` = '$id';
                    ";
        mysqli_query($this->db, $query);
        return true;
    }

    public function deleteTicket($id) {
        $query = "DELETE FROM `ticket`               
                  WHERE `ticket`.`ID` = $id";
        mysqli_query($this->db, $query);
        return true;
    }

}