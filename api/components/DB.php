<?php

final class DB
{

    private static $instance = null;

    private function __construct() {
        include_once('config/db.php');
        $connection = mysqli_connect($db['host'], $db['user'], $db['password'], $db['db_name']);
        mysqli_set_charset($connection, 'utf8');
        self::$instance = $connection;
    }

    public static function getConnection() {
        if (self::$instance === null) {
            new self();
        }
        return self::$instance;
    }

    private function __clone()
    {

    }

    private function __sleep()
    {

    }

    private function __wakeup()
    {

    }

// TODO: посмотреть как работать с БД через PDO
//
//    private static $connection;
//
//    private function __construct()
//    {
//        $db = CONFIG_ROOT . 'db.php';
//        $dsn = "{$db['driver']}:host={$db['host']};dbname={$db['db_name']};charset={$db['charset']}";
//        $opt = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC);
//        $pdo = new PDO($dsn, $db['user'], $db['password'], $opt);
//        self::$connection = $pdo;
//    }
//
//    public static function getInstance()
//    {
//        if (!self::$connection) {
//            new self();
//        }
//        return self::$connection;
//    }
//
//    private static $connection;
//
//    private function __construct()
//    {
//        $db = CONFIG_ROOT . 'db.php';
//        $dsn = "{$db['driver']}:host={$db['host']};dbname={$db['db_name']};charset={$db['charset']}";
//        $opt = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC);
//        $pdo = new PDO($dsn, $db['user'], $db['password'], $opt);
//        self::$connection = $pdo;
//    }

}

?>