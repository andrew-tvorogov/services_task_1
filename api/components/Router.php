<?php


class Router
{

    private $routes;

    public function __construct()
    {
        include_once('config/routes.php');
        $this->routes = $routes;
    }

    public function run()
    {
        $requestedUrl = trim($_SERVER['REQUEST_URI'], '/');
        $urlFound = false; // начальное значение - url не найден
        foreach ($this->routes as $controller => $availableRoutes) {
            foreach ($availableRoutes as $availableRoute => $actionMethod) {
                // $action - это часть url между параметром и именем метода
                // например: http://localhost/cinema/places/hall/1
                // places - метод
                // hall - это actionMethod - дополнительный параметр, прописан в routes
                // 1 - параметр
                // путь: 'places\/hall\/*([0-9]+)' => 'hall'
                if (preg_match("~$availableRoute~", $requestedUrl, $matches)) {					
					// если есть совпадение, то в $id кладём "хвост" - это параметр					
                    $req = isset($matches[1]) ? $matches[1] : 0;
                    $requestedController = new $controller();
                    // в PHP допускается использовать значение переменной в качестве имени функции
                    // поэтому $actionMethod - автоматически подменяется на параметр из routes
                    $requestedController->$actionMethod($req);
                    $urlFound = true;
                    break 2;
                }
            }
        }
        if ($urlFound === false) {
            echo 'Requested url not found - 404';
        }
    }
}