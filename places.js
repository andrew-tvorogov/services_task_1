"use strict";

function contains(arr, elem) {
	return arr.indexOf(elem) != -1;
}

// Get all places by hall id
function getPlacesByHallId(id) {
    let url = global_url + "cinema/api/places/hall/" + id;
    let xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function () {
        let all_places = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
			
            //console.table(all_places);
			daySelect.setAttribute('hidden','hidden'); // отключает поле выбора дня
			
			let rows = [];
			//let numbers = [];
			
			all_places.forEach(function(el){
				if (!contains(rows, el['row'])) {
					//console.log('contains');
					rows.push(el['row']);					
				}				
			});
			//console.log(rows);

				let generated_content = '<div class="hall"><div class="row" style="margin-bottom: 2rem">экран</div>';

				for(let row = 1; row < rows.length - 1; row++){
					generated_content += `<div class="row"><div class="row_marker">ряд: ${row}</div>`;
					all_places.forEach(function(el){
						if(el['row'] == row) {
							generated_content += `
								<div class="seat active">${el['number']}</div>`;
						}
					});
					generated_content += '</div>'; // перевод строки (новый ряд)
				}
			//}

            output.innerHTML = `<h5>Зал: ${ all_places[0]['name_hall'] }</h5>` + generated_content;
			
        } else {
            console.error(all_places);
        }
    }
    xhr.send(null);
}


// Get all places by seance id
function getPlacesBySeanceId(id) {
    let url = global_url + "cinema/api/places/seance/" + id;
    let xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function () {
        let all_places = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
			
            //console.table(all_places);
			daySelect.setAttribute('hidden','hidden');
			
			let rows = [];
			let numbers = [];
			
			all_places.forEach(function(el){
				if (!contains(rows, el['seance_place_row'])) {
					//console.log('contains' + el['seance_place_row']);
					rows.push(el['seance_place_row']);					
				}				
			});

			//console.log('rows: ' + rows);

            let generated_content = '<div class="hall"><div class="row" style="margin-bottom: 2rem">экран</div>';
			for(let row = 1; row < rows.length - 1; row++){
				generated_content += `<div class="row"><div class="row_marker">ряд: ${row}</div>`;
				all_places.forEach(function(el){
					if(el['seance_place_row'] == row){
						if (el['status'] === '2'){
							generated_content += `<div class="seat" style="background-color: red; font-weight: bold;">
													${el['seance_place_number']}
													</div>`;
						} else if (el['status'] === '1'){
							generated_content += `<div class="seat" onclick="deleteTicket(${el['ticket_id']}, ${el['seance_id']})" style="background-color: orange; font-weight: bold;">
													${el['seance_place_number']}
													</div>`;
						} else {
							generated_content += `
								<div class="seat active" onclick="addTicket(
								    ${el['seance_id']},
								    ${el['seance_place_row']},
								    ${el['seance_place_number']})
								    ">${el['seance_place_number']}
								</div>`;
						}						
					}
				});
				generated_content += '</div>'; // первод строки (новый ряд)
			}
			
            output.innerHTML = `<h5>Зал: ${ all_places[0]['seance_hall_name'] }</h5>` + generated_content + `</div>`;
			
        } else {
            console.error(all_places);
        }
    }
    xhr.send(null);
}
