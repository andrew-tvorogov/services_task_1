"use strict";

// Get all cinemas
function getAllCinemas(){
    let url  = global_url + "cinema/api/cinemas";
    let xhr  = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function () {
        let all_cinemas = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            console.table(all_cinemas);

            daySelect.setAttribute('hidden', 'hidden');

			let generated_content = '';
            all_cinemas.forEach(function (el){
				generated_content += `				
				<div class="col p-2">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">${el['name']}</h5>
						<p>${el['address']}</p>
						<button class="btn btn-outline-primary" onclick="getHallsByCinemaId(${el['ID']})">Залы</button>
						<button class="btn btn-outline-secondary" onclick="getSeancesByCinemaId(${el['ID']})">Сеансы</button>
					</div>				
				</div>
				</div>
				`;				
            })
			output.innerHTML = `<div class="row row-cols-2 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 g-3" style="justify-content: start">${generated_content}</div>`;
        } else {
            console.error(all_cinemas);
        }
    }
    xhr.send(null);
}

// Get a cinema
function getOneCinema(ID){
    let url  = global_url + "cinema/api/cinemas";
    let xhr  = new XMLHttpRequest()
    xhr.open('GET', url + '/' + ID, true)
    xhr.onload = function () {
        let one_cinema = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            console.table(one_cinema);
        } else {
            console.error(one_cinema);
        }
    }
    xhr.send(null);
}

// Post a user
function addCinema(){
    let url  = global_url + "cinema/api/cinemas";
    let data = {};
    data.name = "Saint";
    data.address  = "Preston blvd 177";
    let json = JSON.stringify(data);

    let xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
    xhr.onload = function () {
        let cinemas = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            console.table(cinemas);
        } else {
            console.error(cinemas);
        }
    }
    xhr.send(json);
}

// Update a cinema    id=25
function editCinema(id){
    id = 25; // для отладки
    let url  = global_url + "cinema/api/cinemas/";

    let data = {};
    data.name = "Dream";
    data.address  = "Petersburg 12";
    let json = JSON.stringify(data);

    let xhr = new XMLHttpRequest();
    xhr.open("PUT", url+id, true);
    xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
    xhr.onload = function () {
        let cinemas = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            console.table(cinemas);
        } else {
            console.error(cinemas);
        }
    }
    xhr.send(json);
}


// Delete a cinema  id=25
function deleteCinema(id){
    id = 25; // для отладки
    let url  = global_url + "cinema/api/cinemas/";
    let xhr = new XMLHttpRequest();
    xhr.open("DELETE", url + id, true);
    xhr.onload = function () {
        let cinemas = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            console.table(cinemas);
        } else {
            console.error(cinemas);
        }
    }
    xhr.send(null);
}
