// Post a ticket
function addTicket(seance, row, number){
    let url  = global_url + "cinema/api/tickets";

    let data = {};
    data.seance = seance + '';
    data.row  = row + '';
    data.number  = number + '';
    data.status  = "1";

    let json = JSON.stringify(data);
    let xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
    xhr.onload = function () {
        let tickets = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            console.table(tickets);
            getPlacesBySeanceId(seance);
        } else {
            console.error(tickets);
        }
    }
    xhr.send(json);
}

// Delete a ticket
function deleteTicket(id, seance){
    let url  = global_url + "cinema/api/tickets/";
    let xhr = new XMLHttpRequest();
    let request_str = url + id;
    xhr.open("DELETE", request_str, true);
    xhr.onload = function () {
        let tickets = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            console.table(tickets);
            getPlacesBySeanceId(seance);
        } else {
            console.error(tickets);
        }
    }
    xhr.send(null);
}