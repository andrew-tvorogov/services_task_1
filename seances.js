"use strict";

// Get all seances
function getAllSeances() {
    let url = global_url + "cinema/api/seances";
    let xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function () {
        let all_seances = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            //console.table(all_seances);

            daySelect.removeAttribute('hidden');

            let innerContent = '';
            all_seances.forEach(function (el) {
                innerContent += `
                    <tr>
                        <td>${el['datetime']}</td>
                        <td>${el['price']}</td>
                        <td>${el['movie_name']}</td>
                        <td>${el['census']}</td>
                        <td>${el['genre']}</td>
                        <td>${el['hall_name']}</td>
						<td><button onclick=(getPlacesBySeanceId(${el['ID']})) class="btn btn-outline-success">Забронировать</button></td>
                    </tr>`;
            })
            output.innerHTML = `                        
            <table class="table">
            <thead>
                <tr>
                    <th>datetime</th>
                    <th>price</th>
                    <th>movie_name</th>
                    <th>census</th>
                    <th>genre</th>
                    <th>hall_name</th>
					<th></th>
                </tr>
            </thead>
            <tbody>` + innerContent + '</tbody>';
        } else {
            console.error(all_seances);
        }
    }
    xhr.send(null);
}

// Get all seances
function getSeancesByHallId(id) {
    //let url  = `http://localhost/cinema/api/seances/${id}`;
    let url = global_url + "cinema/api/seances";
    let xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function () {
        let all_seances = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            console.table(all_seances);

            let innerContent = '';
            all_seances.forEach(function (el) {
                if (el['hall_id'] == id) {
                    innerContent += `
                    <tr>
                        <td>${el['datetime']}</td>
                        <td>${el['price']}</td>
                        <td>${el['movie_name']}</td>
                        <td>${el['census']}</td>
                        <td>${el['genre']}</td>
                        <td>${el['hall_name']}</td>
						<td><button onclick=(getPlacesBySeanceId(${el['ID']})) class="btn btn-outline-success">Забронировать</button></td>
                    </tr>`;
                }
            })
            output.innerHTML = `
            <table class="table">
            <thead>
                <tr>
                    <th>datetime</th>
                    <th>price</th>
                    <th>movie_name</th>
                    <th>census</th>
                    <th>genre</th>
                    <th>hall_name</th>
					<th></th>
                </tr>
            </thead>
            <tbody>` + innerContent + '</tbody>';

        } else {
            console.error(all_seances);
        }
    }
    xhr.send(null);
}


// Get all days, генерирует список дней
getAllDate();

function getAllDate() {
    let url = global_url + "cinema/api/seances/dates";
    let xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function () {
        let all_date = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            //console.table(all_date);
            let generated_content = '';
            all_date.forEach(function (el) {
                generated_content += `
                    <option value="${el['dates']}">${el['dates']}</option>
                    `;
            })
            sel.innerHTML = generated_content;
        } else {
            console.error(all_date);
        }
    }
    xhr.send(null);
}


// Get all seances by date
function getSeancesByDate(requestedDate) {
    let url = global_url + "cinema/api/seances/date/" + requestedDate;
    let xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function () {
        let all_seances = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            console.table(all_seances);
            daySelect.removeAttribute('hidden');
            let innerContent = '';
            all_seances.forEach(function (el) {
                innerContent += `
                    <tr>
                        <td>${el['datetime']}</td>
                        <td>${el['price']}</td>
                        <td>${el['movie_name']}</td>
                        <td>${el['census']}</td>
                        <td>${el['genre']}</td>
                        <td>${el['hall_name']}</td>
						<td><button onclick=(getPlacesBySeanceId(${el['ID']})) class="btn btn-outline-success">Забронировать</button></td>
                    </tr>`;
            })
            output.innerHTML = `                        
            <table class="table">
            <thead>
                <tr>
                    <th>datetime</th>
                    <th>price</th>
                    <th>movie_name</th>
                    <th>census</th>
                    <th>genre</th>
                    <th>hall_name</th>
					<th></th>
                </tr>
            </thead>
            <tbody>` + innerContent + '</tbody>';
        } else {
            console.error(all_seances);
        }
    }
    xhr.send(null);
}

// Get all seances
function getSeancesByCinemaId(id) {
    let url = global_url + "cinema/api/seances/cinema/" + id;
    let xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function () {
        let all_seances = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            //console.table(all_seances);
            let innerContent = '';
            all_seances.forEach(function (el) {
                innerContent += `
                    <tr>
                        <td>${el['datetime']}</td>
                        <td>${el['price']}</td>
                        <td>${el['movie_name']}</td>
                        <td>${el['census']}</td>
                        <td>${el['genre']}</td>
                        <td>${el['hall_name']}</td>
						<td><button onclick=(getPlacesBySeanceId(${el['ID']})) class="btn btn-outline-success">Забронировать</button></td>
                    </tr>`;
            })
            output.innerHTML = ` 
            <h5>${all_seances[0]['cinema_name']}</h5>                       
            <table class="table">
            <thead>
                <tr>
                    <th>datetime</th>
                    <th>price</th>
                    <th>movie_name</th>
                    <th>census</th>
                    <th>genre</th>
                    <th>hall_name</th>
					<th></th>
                </tr>
            </thead>
            <tbody>` + innerContent + '</tbody>';
        } else {
            console.error(all_seances);
        }
    }
    xhr.send(null);
}
