"use strict";

// Get all halls
function getAllHalls(){    
	let url  = global_url + "cinema/api/halls";
    let xhr  = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function () {
        let all_halls = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {

            daySelect.setAttribute('hidden', 'hidden');

            console.table(all_halls);
            output.innerHTML = '';
			let generated_content = '';
            all_halls.forEach(function (el){
				generated_content += `							
				<div class="col p-2">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">${el['name']}</h5>
						<p style="color: gray"><span style="font-size: .8rem">Название кинотеатра:</span> ${el['name_cinema']}</p>
						<!--p>Количество мест: xx</p-->						
						<button class="btn btn-outline-primary" onclick="getSeancesByHallId(${el['ID']})">						
							Сеансы
						</button>						
						<button class="btn btn-outline-secondary" onclick="getPlacesByHallId(${el['ID']})">
							Схема зала
						</button>
					</div>				
				</div>
				</div>
				`;				               
            })
			output.innerHTML = `<h5>Информация о залах</h5><div class="row row-cols-2 row-cols-sm-2 row-cols-md-3 row-cols-lg-5 g-3">${generated_content}</div>`;
        } else {
            console.error(all_halls);
        }
    }
    xhr.send(null);
}

// Get all halls
function getHallsByCinemaId(id){
    let url  = global_url + "cinema/api/halls";
    let xhr  = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function () {
        let all_halls = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            console.table(all_halls);
            output.innerHTML = '';
			let generated_content = '';
			let halls_cinema_name = '';
            all_halls.forEach(function (el){
                if( id == el['ID_cinema'] ){
					halls_cinema_name = el['name_cinema'];
                    generated_content += `
				<div class="col p-2">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">${el['name']}</h5>
						<p>ID кинотеатра:  ${el['ID_cinema']}</p>
						<!--p>Количество мест: xx</p-->						
						<button class="btn btn-outline-primary" onclick="getSeancesByHallId(${el['ID']})">
							Сеансы
						</button>
						<button class="btn btn-outline-secondary" onclick="getPlacesByHallId(${el['ID']})">
							Схема зала
						</button>						
					</div>				
				</div>
				</div>`									
                }
            })			
			output.innerHTML = `<h5>${halls_cinema_name}</h5><div class="row row-cols-2 row-cols-sm-2 row-cols-md-3 row-cols-lg-5 g-3">${generated_content}</div>`;		
        } else {
            console.error(all_halls);
        }
    }
    xhr.send(null);
}
